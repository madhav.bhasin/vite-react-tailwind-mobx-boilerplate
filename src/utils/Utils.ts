import { isArray, pickBy, random, times } from 'lodash';

export class Utils {
    static getRandomString = (length = 20): string => times(length, () => random(35).toString(36)).join('');

    static sanitizeObject = (data: any) => {
        return pickBy(data, (v: any) => (isArray(v) ? !!v.length : v !== null && v !== undefined && v !== ''));
    };

    static getUniqueId = (): string =>
        (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
}
