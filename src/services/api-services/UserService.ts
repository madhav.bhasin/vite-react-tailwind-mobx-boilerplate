import { ContextStatic } from '@devslane/mobx-entity-manager';
import { UserModel } from 'store/models/UserModel';

class UserService {
    static getInstance(): UserService {
        return new UserService();
    }

    async me(): Promise<UserModel> {
        return await ContextStatic.default.apiService.get('/me').then((res: any) => {
            const me = res?.data || {};
            return UserModel.fromJson({ ...me }) as UserModel;
        });
    }
}

export const userService = UserService.getInstance();
