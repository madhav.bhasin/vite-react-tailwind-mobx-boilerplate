import { EntityBaseModel } from '@devslane/mobx-entity-manager';
import { observable } from 'mobx';
import { UserStore } from 'store/entities/UserStore';

export class UserModel extends EntityBaseModel {
    static _store: UserStore;

    @observable
    first_name!: string;

    @observable
    last_name!: string;
}
