import { action } from 'mobx';
import { ChildStore } from './ChildStore';
import { UserStore } from './entities/UserStore';

export class RootStore {
    private static _instance: RootStore;

    userStore: UserStore;

    private constructor() {
        // We will pass the instance of RootStore in ChildStore for better accessibility of siblings
        this.userStore = new UserStore(this);
    }

    static getInstance(): RootStore {
        if (!this._instance) {
            this._instance = new RootStore();
        }

        return this._instance;
    }

    @action
    logout(): void {
        Object.values(this)
            .filter((v) => v instanceof ChildStore)
            .forEach((v) => (v as ChildStore<any>).reset());
    }
}
