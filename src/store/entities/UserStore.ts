import { action, observable } from 'mobx';
import { ChildStore } from '../ChildStore';
import { Context } from '../Context';
import { UserModel } from '../models/UserModel';
import { RootStore } from '../RootStore';
import { userService } from '../../services/api-services/UserService';

export enum LoginStatus {
    LOGGED_IN,
    LOGGED_OUT,
    PENDING,
}

export class UserStore extends ChildStore<UserModel> {
    constructor(rootStore: RootStore) {
        super(rootStore);
        UserModel._store = this;
    }

    @observable loginStatus!: LoginStatus;
    @observable loggedInUser?: UserModel;

    reset() {
        this.setLoginStatus(LoginStatus.LOGGED_OUT);
        this.loggedInUser = undefined;
        this.entities.clear();
        Context.storage.reset();
    }

    hasLoggedInToken = async () => {
        return !!(await Context.storage.getAuthToken());
    };

    getLoggedInToken = async () => {
        return await Context.storage.getAuthToken();
    };

    @action
    async me(): Promise<UserModel> {
        try {
            const user = await userService.me();
            this.setLoggedInUser(user);
            return user;
        } catch (e) {
            console.error(e);
            throw e;
        }
    }

    @action
    setLoggedInUser(data: UserModel): void {
        this.loggedInUser = UserModel.fromJson(data) as UserModel;
        this.setLoginStatus(LoginStatus.LOGGED_IN);
    }

    @action
    setLoginStatus(status: LoginStatus) {
        this.loginStatus = status;

        if (status === LoginStatus.LOGGED_OUT) {
            this.loggedInUser = undefined;
            Context.storage.reset();
        }
    }
}
