import { AppContext } from 'App.context';
import envConfig from 'Env.config';
import React from 'react';
import ReactDOM from 'react-dom';
import { StorageService } from 'services/StorageService';
import { ToastService } from 'services/ToastService';
import { Context } from 'store/Context';
import App from './App';
import './index.scss';

Context.boot({
    storageService: StorageService.getInstance(),
    baseUrl: envConfig.BACKEND_URL as string,
    toastService: ToastService.getInstance(),
});

AppContext.boot();

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root'),
);
