import { useEffect } from 'react';

const useDisableScroll = (disabled: boolean) => {
    useEffect(() => {
        document.body.style.overflowY = disabled ? 'hidden' : 'scroll';

        return () => {
            document.body.style.overflowY = 'scroll';
        };
    }, [disabled]);
};

export default useDisableScroll;
