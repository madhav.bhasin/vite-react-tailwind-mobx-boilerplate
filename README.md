# React Tailwindcss MobX Boilerplate build with Vite

This is a [ReactJS](https://reactjs.org) + [Vite](https://vitejs.dev) boilerplate to be used with [Tailwindcss](https://tailwindcss.com), [MobX](https://mobx.js.org/README.html).

## What is inside?

This project uses many tools like:

-   [ReactJS](https://reactjs.org)
-   [Vite](https://vitejs.dev)
-   [TypeScript](https://www.typescriptlang.org)
-   [Tailwindcss](https://tailwindcss.com)
-   [MobX](https://mobx.js.org/README.html)
-   [MobX Entity Manager](https://www.npmjs.com/package/@devslane/mobx-entity-manager)
-   [Eslint](https://eslint.org)
-   [Prettier](https://prettier.io)

## Getting Started

### Install

Create the project.

```bash
npx degit https://github.com/madhav-devslane/vite-react-tailwind-boilerplate my-app
```

Access the project directory.

```bash
cd my-app
```

Install dependencies.

```bash
yarn
```

Serve with hot reload at http://localhost:3000.

```bash
yarn run dev
```

### Lint

```bash
yarn run lint
```

### Details

-   How to use .env variables ?

    -   Add env variables to .env file (use .env.[development/production] as per your current environment)

    -   Use `VITE_` prefix to your env variables to let vite compiler know that these variables can be publicly accessed.

    -   _This is as per personal choice._ Use a separate env constant file to export the env variables. (Vite uses not a good to eye method to use the env variables :) ). See example below

```js
const envConfig = {
    BACKEND_URL: import.meta.env.VITE_REACT_APP_BACKEND_URL!,
};

export default envConfig;
```

-   How to use AppContext (mobx store, toast service, storage service) ?

    -   **MobX Store** or store (as per AppContext) is a instance of the RootStore which we can use to access any child store like _userStore_ in the below example. Create child store, Add the same to root store , use the root store instance to access that particular child store

    -   **Toast Service** is nothing , just a service to use notifications. It is better that we configure methods like showSuccess , showError etc. at a single place. So that we don't need to specify every time what color, position, time for the notification. For specific cases we can pass more configurations to the functions to override default values

    -   **Storage Service** is used to access/use browser level storage like localStorage, sessionStorage, cookies etc. By Default I have configured localStorage. We can change/extend the same as per our usage

**Note 1 : Not just these 3 services , we can add more services that have global level usage to the AppContext. One Example is socketService**

**Note 2 : Don't forget to use _observer_ in your component :expressionless:**

```js
import { AppContext } from 'App.context';

const Login: React.FC = () => {
    const { store, toast, storage } = AppContext;

    const handleLogin = async (email: string, password: string) => {
        try {
            await store.userStore.login({ email, password });
            toast.showSuccess('Logged In Successfully');
            //Route to logged in page after this
        } catch (e) {
            /* If we are using AdonisJs as BE framework, 
          then we need to make sure if the error 
          is validation error or service error
          In such cases it is better to use a BaseErrorClass 
          that will map the BE error to a specific schema 
          for FE to understand if it is a validation error 
          or general service based error */
            toast.showError(e?.message ?? 'SOMETHING BAD HAPPENED');

            /* Here storage is localStorage. You can use 
          cookies/session as per your usage or extend 
          the same service to use multiple */
            storage.clearAll();
        }
    };
};
```

-   **Bonus: BaseError class for FE that use AdonisJS as BE framework. Use this is the BaseApiService :wink:. So that you always get a specific error schema**

```js
import { AxiosError } from 'axios';

export enum ErrorCode {
    UNIDENTIFIED,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    UNPROCESSABLE_ENTITY = 422,
    TOO_MANY_REQUESTS = 429,
}

export type validationErrorType = { field: string; message: string; rule: string };

export class BaseError {
    constructor(
        readonly message: string = 'Error. Please Try Again.',
        readonly code?: any,
        readonly status: ErrorCode = ErrorCode.UNIDENTIFIED,
        readonly error_code?: any,
        readonly errors: validationErrorType[] = [],
        readonly data: any = [],
    ) {}

    static fromJSON(json: AxiosError) {
        return BaseError.from(json);
    }

    static toJSON(json: BaseError) {
        return {
            message: json.message || 'Error. Please Try Again.',
            code: json.code || -1,
            error_code: json.error_code || -1,
            status: json.status || -1,
            errors: json.errors || [],
            data: json.data || [],
        };
    }

    static from(axiosError: AxiosError): BaseError {
        if (!axiosError.response) {
            return new BaseError();
        }

        const { status, data } = axiosError.response;
        return new BaseError(
            data?.message,
            data?.code,
            status,
            data?.error_code,
            data?.errors || [],
            data?.data || [],
        );
    }

    isValidationError(): boolean {
        return (this.errors || []).length > 0;
    }

    errorsByKey(key: string): validationErrorType | undefined {
        return this.errors?.find((er: validationErrorType) => er.field === key);
    }

    hasErrorByKey(key: string): boolean {
        return !!this.errorsByKey(key);
    }
}
```

-   **What if I have my own font files ?**

    -   Add font files in public folder. I have created another folder `font-files` in public and copied all the font files into that
    -   Use those files in your global css file to override the defaults. See below

```css
@font-face {
    font-family: 'GT Walsheim Pro Regular';
    font-style: regular;
    src: url('/font-files/GT-WALSHEIM-PRO-REGULAR.TTF') format('truetype'), url('/font-files/GTWalsheimProRegular.woff2')
            format('woff2');
    font-display: swap;
}

html {
    font-family: 'GT Walsheim Pro Regular';
}
```
